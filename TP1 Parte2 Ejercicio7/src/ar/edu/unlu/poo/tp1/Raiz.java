package ar.edu.unlu.poo.tp1;

import java.util.ArrayList;

public class Raiz {
    private Double raiz1 = null;
    private Double raiz2 = null;

    private void setRaiz1(Double raiz){
        this.raiz1=raiz;
    }
    private void setRaiz2(Double raiz){
        this.raiz2=raiz;
    }
    private Double getRaiz1(){
        return this.raiz1;
    }
    private Double getRaiz2(){
        return this.raiz2;
    }
    int cuantasRaicesTiene(){
       return consultarRaices().size();
    }
    void cargarRaices(Double raiz1, Double raiz2){
        setRaiz1(raiz1);
        setRaiz2(raiz2);
    }
    ArrayList<String> consultarRaices (){
        ArrayList<String> respuesta = new ArrayList<>();
        if (raiz1 != null){
            respuesta.add(String.valueOf(raiz1));
        }
        if (raiz2 != null){
            respuesta.add(String.valueOf(raiz2));
        }
        if (!(raiz1 == null || raiz2 == null)){
            if (raiz1.equals(raiz2)){
                respuesta.remove(1);
            }
        }
        return respuesta;
    }
}
