package ar.edu.unlu.poo.tp1;

import java.util.Scanner;

public class Aplicacion {
    private Ecuacion ecuacion = new Ecuacion();
    Scanner sc = new Scanner(System.in);
    public void cargarValoresEcuacion(){
        System.out.println("");
        System.out.println("-----------------------------------------------------------");
        System.out.println("A CONTINUACION CARGAREMOS LOS 3 VALORES DE LA ECUACION");
        System.out.println("-----------------------------------------------------------");
        System.out.println("");
        System.out.println("INGRESE EL PRIMER VALOR QUE ACOMPAÑA A X^2.");
        Double num1 = Double.valueOf(sc.nextLine());
        String op1="";
        if (num1 == 0){
            op1 = "";
        }else if (num1 == 1){
            op1 = "X^2 ";
        }else if (num1 == -1){
            op1 = "-X^2 ";
        } else if (num1 < 0 && num1 != -1) {
            op1 = num1+"X^2 ";
        } else if (num1>0 && num1 != 1) {
            op1 = num1 + "X^2 ";
        }
        System.out.println("");
        System.out.println("INGRESE EL SEGUNDO VALOR QUE ACOMPAÑA A X.");
        Double num2 = Double.valueOf(sc.nextLine());
        String op2="";
        if (num2 == 0){
            op2 = "";
        }else if (num2 == 1){
            op2 = "X ";
        }else if (num2 == -1){
            op2 = "-X ";
        } else if (num2 < 0 && num2 != -1) {
            op2 = num2+"X ";
        } else if (num2>0 && num2 != 1) {
            op2 = "+"+num2 + "X ";
        }
        System.out.println("");
        System.out.println("INGRESE EL TERCER VALOR.");
        Double num3 = Double.valueOf(sc.nextLine());
        String op3="";
        if (num3 == 0){
            op3 = "";
        }else if (num3 == 1){
            op3 = "+1";
        }else if (num3 == -1){
            op3 = "-1";
        } else if (num3 < 0 && num3 != -1) {
            op3 = String.valueOf(num3);
        } else if (num3>0 && num3 != 1) {
            op3 = "+"+num3;
        }
        System.out.println("");
        System.out.println("USTED INGRESO LA ECUACION: " +op1+op2+op3);
        ecuacion.cargarValores(num1,num2,num3);
        System.out.println("");
        System.out.println("INGRESE UN VALOR PARA LA X: ");
        Double op4 = Double.valueOf(sc.nextLine());
        System.out.println("");
        System.out.println("El polinomio tiene " + ecuacion.cuantasRaicesTiene() + " Raices.");
        if (ecuacion.cuantasRaicesTiene() == 1){
            System.out.println("Tiene una sola raiz: " + ecuacion.consultarRaices().get(0));
        }
        if (ecuacion.cuantasRaicesTiene() == 2){
            System.out.println("La primera raiz es: " + ecuacion.consultarRaices().get(0));
            System.out.println("La segunda raiz es: " + ecuacion.consultarRaices().get(1));
        }
        System.out.println("El valor de Y=" + ecuacion.calcularValorY(op4));
    }
}
