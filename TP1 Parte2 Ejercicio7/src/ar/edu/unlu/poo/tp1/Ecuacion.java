package ar.edu.unlu.poo.tp1;

import java.util.ArrayList;

public class Ecuacion {
    private double valor1;
    private double valor2;
    private double valor3;
    private Raiz raiz = new Raiz();

    private void setValor1(Double valor){
        this.valor1 = valor;
    }
    private void setValor2(Double valor){
        this.valor2=valor;
    }
    private void setValor3(Double valor){
        this.valor3=valor;
    }
    private double getValor1(){
        return this.valor1;
    }
    private double getValor2(){
        return this.valor2;
    }
    private double getValor3(){
        return this.valor3;
    }
    void cargarValores(Double valor1,Double valor2, Double valor3){
        setValor1(valor1);
        setValor2(valor2);
        setValor3(valor3);
        calcularRaices();
    }
    private Raiz calcularRaices(){
        double resultado1 = ((Math.pow(valor2,2))-(4*valor1*valor3));
        if (resultado1>=0){
            raiz.cargarRaices(((-1*valor2)+resultado1)/(2*valor1), ((-1*valor2)-resultado1)/(2*valor1));
        }
        return raiz;
    }
    int cuantasRaicesTiene(){
        return raiz.cuantasRaicesTiene();
    }
    ArrayList<String> consultarRaices(){
        return raiz.consultarRaices();
    }
    Double calcularValorY(Double x){
        return  ( (getValor1()*(Math.pow(x,2))) + (getValor2()*x) + getValor3());

    }
}
