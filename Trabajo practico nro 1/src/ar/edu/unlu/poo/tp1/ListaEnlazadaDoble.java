package ar.edu.unlu.poo.tp1;

public class ListaEnlazadaDoble {
    private Nodo puntero = null;

    public void agregar(Object dato){
        Nodo nodoNuevo = new Nodo();
        nodoNuevo.setDato(dato);
        Nodo nodoAux = puntero;
        if (puntero == null){
            puntero = nodoNuevo;
        } else {
            while (nodoAux != null){
                if (nodoAux.getProximo() == null ){
                    nodoNuevo.setAnterior(nodoAux);
                    nodoAux.setProximo(nodoNuevo);
                    nodoAux=null;
                }else {
                    nodoAux = nodoAux.getProximo();
                }
            }
        }
    }
    public boolean eliminar(int ubicacion){
        boolean respuesta = false;
        Nodo nodoAux = puntero;
        if (nodoAux == null){
            respuesta = false;
        } else if ((ubicacion == 1) && (nodoAux != null) ) {
            if (nodoAux.getProximo() != null) {
                puntero = nodoAux.getProximo();
                puntero.setAnterior(null);
                respuesta = true;
            }else{
                puntero = null;
                respuesta = true;
            }
        } else{
            int contador = 1;
            while(nodoAux != null){
                if (contador == ubicacion){
                    nodoAux.getAnterior().setProximo(nodoAux.getProximo());
                    nodoAux.getProximo().setAnterior(nodoAux.getAnterior());
                    nodoAux = nodoAux.getProximo();
                    contador++;
                    respuesta = true;
                }
                nodoAux = nodoAux.getProximo();
                contador++;
            }
        }
        return respuesta;
    }
    public boolean insertar(int ubicacion, Object dato){
        boolean respuesta = false;
        Nodo nodoNuevo = new Nodo();
        nodoNuevo.setDato(dato);
        Nodo nodoAux = puntero;
        if (nodoAux == null && ubicacion == 1){
            puntero = nodoNuevo;
            respuesta = true;
        }else if (nodoAux != null && ubicacion == 1) {
            puntero = nodoNuevo;
            puntero.setProximo(nodoAux);
            nodoAux.setAnterior(puntero);
            respuesta = true;
        }else if (ubicacion <1 || ubicacion > length()+1) {
            respuesta = false;
        }else if (ubicacion == length()+1) {
            agregar(nodoNuevo);
        }else {
            int contador = 1;
            while (nodoAux != null){
                if (contador == ubicacion){
                    nodoNuevo.setProximo(nodoAux);
                    nodoNuevo.setAnterior(nodoAux.getAnterior());
                    nodoAux.getAnterior().setProximo(nodoNuevo);
                    nodoAux.setAnterior(nodoNuevo);
                    nodoAux=null;
                    respuesta = true;
                }else {
                    nodoAux = nodoAux.getProximo();
                    contador++;
                }
            }
        }
        return respuesta;
    }
    public Object recuperar(int ubicacion){
        Object respuesta = "";
        if (ubicacion<1 || ubicacion > length()){
            return "";
        }else {
            Nodo nodoAux = puntero;
            if (nodoAux == null) {
                respuesta = "";
            } else {
                int contador = 1;
                while (nodoAux != null) {
                    if (ubicacion == contador) {
                        respuesta = nodoAux.getDato();
                        nodoAux = null;
                    } else {
                        nodoAux = nodoAux.getProximo();
                        contador++;
                    }
                }
            }
        }
        return respuesta;
    }
    public int length (){
        int respuesta= 0;
        int contador = 1;
        Nodo nodoAux = puntero;
        while (nodoAux != null){
            respuesta = contador;
            nodoAux = nodoAux.getProximo();
            contador++;
        }
        return respuesta;
    }
    public boolean esVacia (){
        if (puntero == null){
            return true;
        }else{
            return false;
        }
    }
}
