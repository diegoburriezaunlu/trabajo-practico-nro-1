package ar.edu.unlu.poo.tp1;

public class Cola {
    private Nodo primero = null;
    private Nodo ultimo = null;

    public void encolar (Object dato){
        Nodo nodoNuevo = new Nodo();
        nodoNuevo.setDato(dato);
        if (esVacia()){
            primero = nodoNuevo;
            ultimo = nodoNuevo;
        } else if (ultimo.equals(primero)){
            ultimo = nodoNuevo;
            ultimo.setProximo(primero);
            primero.setAnterior(ultimo);
        }else{
            nodoNuevo.setProximo(ultimo);
            ultimo.setAnterior(nodoNuevo);
            ultimo = nodoNuevo;
        }
    }
    //Saca el elemento de la cola y devuelve el objeto que estaba encolado primero en la cola.
    public Object desencolar (){
        Nodo nodoAux;
        if (esVacia()) {
            return "";
        }else if (primero.equals(ultimo)){
            nodoAux = primero;
            primero = null;
            ultimo = null;
            return nodoAux.getDato();
        }else{
            nodoAux = primero;
            primero = primero.getAnterior();
            primero.setProximo(null);
            return nodoAux.getDato();
        }
    }
    public boolean esVacia (){
        if (primero == null){
            return true;
        }else{
            return false;
        }
    }
    //Devuelve el objeto de la cola pero sin sacarlo de la cola.
    public Object recuperarSinDesencolar (){
            if (esVacia()){
                return "";
            }else{
                return primero.getDato();
            }
    }
}
