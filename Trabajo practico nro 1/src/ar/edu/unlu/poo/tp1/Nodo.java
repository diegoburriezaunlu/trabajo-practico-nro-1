package ar.edu.unlu.poo.tp1;

public class Nodo {
    private Nodo anterior;
    private Nodo proximo;
    private Object dato;

    public void setAnterior(Nodo nodo){
        this.anterior = nodo;
    }
    public void setProximo(Nodo nodo){
        this.proximo = nodo;
    }
    public void setDato (Object dato){
        this.dato = dato;
    }
    public Nodo getAnterior (){
        return anterior;
    }
    public Nodo getProximo (){
        return proximo;
    }
    public Object getDato(){
        return dato;
    }
}
