package ar.edu.unlu.poo.tp1;

public class ListaEnlazadaSimple {
    private Nodo puntero = null;


    public void agregar(Object dato){
        Nodo nuevoNodo = new Nodo();
        nuevoNodo.setDato(dato);
        Nodo nodoAux = puntero;
        if (nodoAux == null){
            puntero = nuevoNodo;
        } else {
            while(nodoAux.getProximo() != null){
                nodoAux = nodoAux.getProximo();
            }
            nodoAux.setProximo(nuevoNodo);
        }
    }
    public boolean eliminarElemento(int ubicacion){
        boolean respuesta = false;
        Nodo nodoAux = puntero;
        Nodo anterior = null;
        if (nodoAux == null || ubicacion < 1) {
            respuesta = false;
        }else{
            if(ubicacion == 1) {
                puntero = puntero.getProximo();
                respuesta = true;
            }else{
                int contador = 1;
                while(nodoAux != null){
                    if (contador == (ubicacion -1)){
                        anterior = nodoAux;
                    } else if (contador == ubicacion && nodoAux.getProximo()==null) {
                        anterior.setProximo(null);
                        respuesta = true;
                    }else if (contador == (ubicacion + 1)){
                        anterior.setProximo(nodoAux);
                        respuesta = true;
                    }
                    nodoAux = nodoAux.getProximo();
                    contador++;
                }
            }
        }
        return respuesta;
    }
    public boolean insertar(int ubicacion, Object dato){
        Nodo nodoNuevo = new Nodo();
        nodoNuevo.setDato(dato);
        Nodo nodoAux = puntero;
        boolean respuesta = false;
        if (ubicacion < 1 || nodoAux == null){
            respuesta = false;
        }else if (ubicacion == 1 && nodoAux == null){
            puntero = nodoNuevo;
            respuesta = true;
        }else if (ubicacion == 1 && nodoAux != null){
            nodoNuevo.setProximo(puntero);
            puntero = nodoNuevo;
            respuesta = true;
        }else{
            int contador = 1;
            Nodo anterior = null;
            while (nodoAux != null){
                if (contador == (ubicacion -1)){
                    nodoNuevo.setProximo(nodoAux.getProximo());
                    nodoAux.setProximo(nodoNuevo);
                    respuesta = true;
                }
                nodoAux = nodoAux.getProximo();
                contador++;
            }
        }
        return respuesta;
    }
    public Object recuperar(int ubicacion){
        Object respuesta ="";
        Nodo nodoAux = puntero;
        if (ubicacion < 1 || nodoAux == null){
            //respuesta = "La ubicacion no existe o lista vacia";
        } else if (ubicacion == 1) {
            respuesta = nodoAux.getDato();
        } else {
            int contador = 1;
            while (nodoAux != null){
                if (contador == ubicacion){
                    respuesta = nodoAux.getDato();
                    nodoAux = nodoAux.getProximo();
                    contador++;
                }else{
                    nodoAux = nodoAux.getProximo();
                    contador++;
                }
            }
            //if (respuesta == "") {
                //respuesta = "No existe la ubicacion";
            //}
        }
        return respuesta;
    }

    public int length (){
        int contador = 0;
        Nodo nodoAux = puntero;
        while( nodoAux != null){
            contador++;
            nodoAux = nodoAux.getProximo();
        }
        return contador;
    }
    public boolean esVacia (){
        if (puntero == null){
            return true;
        } else {
            return false;
        }
    }
}
