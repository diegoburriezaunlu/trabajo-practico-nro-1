package ar.edu.unlu.poo.tp1;

public class Pila {
    private Nodo topeDePila = null;
    //Apila un nuevo elemento en el tope de la pila.
    public void apilar(Object dato){
        Nodo nodoNuevo = new Nodo();
        nodoNuevo.setDato(dato);
        Nodo nodoAux = topeDePila;
        if (nodoAux == null){
            topeDePila = nodoNuevo;
        }else{
            nodoNuevo.setAnterior(topeDePila);
            topeDePila = nodoNuevo;
        }
    }
    //Desapila el elemento encontrado en el tope de la pila, descontando un elemento a la pila.
    //Cuando lo desapila lo devuelve.
    public Object desapilar(){
        Object respuesta = "";
        if (topeDePila != null){
            respuesta = topeDePila.getDato();
            topeDePila = topeDePila.getAnterior();
        }
        return respuesta;
    }
    //Consulta si la pila es vacia y devuelve un boolean.
    public boolean esVacia(){
        if (topeDePila == null){
            return true;
        }else{
            return false;
        }
    }
    //Recupera el tope de la pila, sin sacarlo de la pila.
    public Object recuperarSinDesapilar(){
        if (topeDePila == null){
            return "";
        }else{
            return topeDePila.getDato();
        }
    }
}
