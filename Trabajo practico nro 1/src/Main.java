import ar.edu.unlu.poo.tp1.Cola;
import ar.edu.unlu.poo.tp1.ListaEnlazadaDoble;
import ar.edu.unlu.poo.tp1.ListaEnlazadaSimple;
import ar.edu.unlu.poo.tp1.Pila;

public class Main {
    public static void main(String[] args) {
        System.out.println("************PRUEBA CLASE LISTA SIMPLES************");
        //Crear Lista.
        ListaEnlazadaSimple listaSimple = new ListaEnlazadaSimple();

        //Pregunto y muestro si es vacia.
        if (listaSimple.esVacia()){
            System.out.println("Esta vacia");
        }else{
            System.out.println("Tiene elementos");
        }

        //Consulto la longitud de la lista.
        System.out.println("La longitud de la lista es " + listaSimple.length());

        // Agregar elemento al final de la lista.
        listaSimple.agregar("Un elemento nuevo");
        System.out.println("Luego de agregar la longitud de la lista es " + listaSimple.length());

        //Elimino elemento.
        listaSimple.eliminarElemento(1);
        System.out.println("Luego de eliminar la longitud de la lista es " + listaSimple.length());

        //Recuperar elemento
        listaSimple.agregar("TP");
        listaSimple.agregar("01");
        listaSimple.agregar("en proceso");
        System.out.println(listaSimple.recuperar(1));
        System.out.println("Luego de recuperar la longitud de la lista es " + listaSimple.length());

        //Insertar elemento en posicion especifica.
        listaSimple.insertar(3,"Nuevo elemento insertado");
        System.out.println("Luego de insertar la longitud de la lista es " + listaSimple.length());

        System.out.println("*********************************************************");
        System.out.println("************PRUEBA CLASE LISTA DOBLE************");

        //Crear Lista.
        ListaEnlazadaDoble listaDoble = new ListaEnlazadaDoble();

        //Pregunto y muestro si es vacia.
        if (listaDoble.esVacia()){
            System.out.println("Esta vacia");
        }else{
            System.out.println("Tiene elementos");
        }

        //Consulto la longitud de la lista.
        System.out.println("La longitud de la lista es " + listaDoble.length());

        // Agregar elemento al final de la lista.
        listaDoble.agregar("Un elemento nuevo");
        System.out.println("Luego de agregar la longitud de la lista es " + listaDoble.length());

        //Elimino elemento.
        listaDoble.eliminar(1);
        System.out.println("Luego de eliminar la longitud de la lista es " + listaDoble.length());

        //Recuperar elemento
        listaDoble.agregar("TP");
        listaDoble.agregar("01");
        listaDoble.agregar("en proceso");
        System.out.println(listaDoble.recuperar(3));
        System.out.println("Luego de recuperar la longitud de la lista es " + listaDoble.length());

        //Insertar elemento en posicion especifica.
        listaDoble.insertar(3,"Nuevo elemento insertado");
        System.out.println("Luego de insertar la longitud de la lista es " + listaDoble.length());

        System.out.println("*********************************************************");
        System.out.println("************PRUEBA CLASE PILA************");


        //Crear Pila.
        Pila pila = new Pila();

        //Pregunto y muestro si es vacia.
        if (pila.esVacia()){
            System.out.println("Esta vacia");
        }else{
            System.out.println("Tiene elementos");
        }

        // Agregar elemento a la pila.
        pila.apilar("Un elemento nuevo");
        System.out.println("Se apilo este elemento: " + pila.recuperarSinDesapilar());

        //Pregunto y muestro si es vacia.
        if (pila.esVacia()){
            System.out.println("Esta vacia");
        }else{
            System.out.println("Tiene elementos");
        }

        //Desapilar eliminando el elemento que desapilo de la pila.
        System.out.println(pila.desapilar());

        //Recuperar elemento
        pila.apilar("TP");
        pila.apilar("01");
        pila.apilar("en proceso");
        System.out.println(pila.desapilar());

        //Pregunto y muestro si es vacia.
        if (pila.esVacia()){
            System.out.println("Esta vacia");
        }else{
            System.out.println("Tiene elementos");
        }

        System.out.println("*********************************************************");
        System.out.println("************PRUEBA CLASE COLA************");


        //Crear Cola.
        Cola cola = new Cola();

        //Pregunto y muestro si es vacia.
        if (cola.esVacia()){
            System.out.println("Esta vacia");
        }else{
            System.out.println("Tiene elementos");
        }

        // Agregar elemento a la cola.
        cola.encolar("Un elemento nuevo");
        System.out.println("Se encolo este elemento: " + cola.recuperarSinDesencolar().toString());

        //Pregunto y muestro si es vacia.
        if (cola.esVacia()){
            System.out.println("Esta vacia");
        }else{
            System.out.println("Tiene elementos");
        }

        //Desencolo eliminando el elemento de la cola.
        System.out.println("Elimino este elemento de la cola: " + cola.desencolar());

        //Pregunto y muestro si es vacia.
        if (cola.esVacia()){
            System.out.println("Esta vacia");
        }else{
            System.out.println("Tiene elementos");
        }

        //Recuperar elemento
        cola.encolar("TP");
        cola.encolar("01");
        cola.encolar("en proceso");
        System.out.println(cola.desencolar());

        //Pregunto y muestro si es vacia.
        if (cola.esVacia()){
            System.out.println("Esta vacia");
        }else{
            System.out.println("Tiene elementos");
        }

        System.out.println("*********************************************************");
    }
}