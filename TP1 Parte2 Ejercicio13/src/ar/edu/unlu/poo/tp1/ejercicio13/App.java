package ar.edu.unlu.poo.tp1.ejercicio13;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;


public class App {
    private List<Tarea> tareas = new ArrayList<>();
    private ArrayList<String> colaboradores = new ArrayList<>();
    private String colaborador;
    private Scanner sc = new Scanner(System.in);

    public void mostarMenu(){
        Scanner sc = new Scanner(System.in);
        int opcion;
        boolean salir = false;
        agregar("Ir al supermercado mañana", Prioridad.ALTA,LocalDate.now(), false, LocalDate.parse("2023-08-28"));
        agregar("Consultar repuesto del auto", Prioridad.MEDIA,LocalDate.now().minusDays(1), true,LocalDate.parse("2023-08-27"));
        agregar("Ir al cine a ver la nueva película de Marvel", Prioridad.BAJA,LocalDate.now().minusDays(1), false,LocalDate.parse("2023-08-26"));
        tareas.get(1).setColaborador("Juan");
        tareas.get(1).setFechaDeFinalizacion(LocalDate.parse("2023-08-31"));
        ingresoColaborador();
        while (!salir) {
            System.out.println("");
            System.out.println("<COLABORADOR> ---> "+colaborador);
            System.out.println("****************************RECORDATORIOS***********************************");
            System.out.println(recordatorio());
            System.out.println("****************************************************************************");
            System.out.println("              1. Mostrar tareas");
            System.out.println("              2. Agregar una tarea");
            System.out.println("              3. Modificar descripcion");
            System.out.println("              4. Modificar prioridad");
            System.out.println("              5. Elejir una tareas y marcar como FINALIZADA/INCONCLUSA");
            System.out.println("              6. Consultar ESTADO de las TAREAS");
            System.out.println("              7. Consultar cuales estan VENCIDAS");
            System.out.println("              8. MOSTRAR tareas ordenadas por PRIORIDAD");
            System.out.println("              9. MOSTRAR tareas ordenadas por fecha de VENCIMIENTO");
            System.out.println("              10.BUSCAR por titulo y marcar como FINALIZADA");
            System.out.println("              11.Listado de tareas FINALIZADAS por COLABORADOR");
            System.out.println("              0. Salir");
            System.out.println("****************************************************************************");
            System.out.println("Elija una de las opciones");

            opcion = sc.nextInt();

            switch (opcion) {
                case 0:
                    salir = true;
                    break;
                case 1:
                    System.out.println(mostrarTarea());
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 2:
                    agregarTarea();
                    break;
                case 3:
                    modificarDescripcion();
                    break;
                case 4:
                    modificarPrioridad();
                    break;
                case 5:
                    modificarEstado();
                    break;
                case 6:
                    System.out.println(mostrarEstado());
                    sc.nextLine();
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    break;
                case 7:
                    System.out.println(mostrarvencida());
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 8:
                    System.out.println(mostrarTareaNoVencidadPrioridad());
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 9:
                    System.out.println(mostrarTareaNoVencidadVencimiento());
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 10:
                    marcarComoFinalizada();
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 11:
                    listadoPorColaborador();
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                default:
                    System.out.println("Solo numeros entre 0 y 9");

            }
        }
    }
    private void listadoPorColaborador(){
        System.out.println("Los colaboradores son: ");
        for (int i=0;i<colaboradores.size();i++){
            System.out.println((i+1) +" --> " + colaboradores.get(i));
        }
        System.out.println("INGRESE el nro de colaborador para imprimir la lista");
        int opc = sc.nextInt();
        System.out.println("COLABORADOR ---> " + colaboradores.get((opc-1)));
        for (int i=0;i<tareas.size();i++){
            if (!tareas.get(i).getColaborador().isEmpty()){
                if (tareas.get(i).getColaborador().equals(colaboradores.get(opc-1))){
                    System.out.println(tareas.get(i).getDescripcion() + " - " + tareas.get(i).getFechaDeFinalizacion());
                }
            }
        }
    }
    private void ingresoColaborador(){
        System.out.println("Los colaboradores son: ");
        for (int i=0;i<tareas.size();i++){
            if (!tareas.get(i).getColaborador().isEmpty()){
                if(colaboradores.size()==0){
                    colaboradores.add(tareas.get(i).getColaborador());
                }else{
                    for (int j=0;j<colaboradores.size();j++){
                        boolean flag=false;
                        if(tareas.get(i).getColaborador().equals(colaboradores.get(j))){
                            flag=true;
                        }
                        if(!flag) {
                            colaboradores.add(tareas.get(i).getColaborador());
                        }
                    }
                }
            }
        }
        for (int i=0;i<colaboradores.size();i++){
            System.out.println(colaboradores.get(i));
        }
        System.out.println("");
        System.out.println("Ingrese su nombre, aunque no este entre los colaboradores: ");
        colaborador = sc.nextLine();
        boolean igual =false;
        for (int i=0;i<colaboradores.size();i++){
            if (colaborador.equals(colaboradores.get(i))){
                igual =true;
            }
        }
        if (!igual){
            colaboradores.add(colaborador);
        }
    }
    private Tarea buscarTareaTitulo(String tarea){
        Tarea respuesta=null;
        for (int i=0;i<tareas.size();i++){
            if (tarea.equals(tareas.get(i).getDescripcion())){
                respuesta = tareas.get(i);
            }
        }
        return respuesta;
    }
    private void marcarComoFinalizada(){
        System.out.println("Ingrese el titulo de la tarea que quiere marcar FINALIZADA: ");
        Tarea tarea = new Tarea();
        tarea = buscarTareaTitulo(sc.nextLine());
        String flag="";
        while (tarea == null && !flag.equals("x")){
            System.out.println("LA TAREA NO PUDO SER ENCONTRADA");
            System.out.println("PARA SALIR INGRESE LA LETRA: x" );
            System.out.println("Ingrese el titulo de la tarea que quiere marcar FINALIZADA: ");
            flag = sc.nextLine();
            tarea = buscarTareaTitulo(flag);
        }
        if (tarea != null){
            tarea.setEstadoCompleto(true);
            tarea.setColaborador(colaborador);
            tarea.setFechaDeFinalizacion(LocalDate.now());
        }
    }
    private String recordatorio(){
        String respuesta ="";
        int cont=0;
        respuesta = String.format("%-15s %-45s %15s","AVISO","TAREA","PRIORIDAD"+"\n");
        for (int i=0;i<tareas.size();i++){
            if(tareas.get(i).consultaRecordatorio()){
                tareas.get(i).setPrioridad(Prioridad.ALTA);
                respuesta += String.format("%-15s %-45s %15s","Por vencer",tareas.get(i).getDescripcion(),tareas.get(i).getPrioridad()+"\n");
                cont++;
            }
        }
        if (cont == 0){
            respuesta = "           SIN TAREAS POR VENCER";
        }
        return respuesta;
    }

    private String mostrarTareaNoVencidadPrioridad(){
        String consola="";
        ArrayList<Tarea> tareaNoVencidas = new ArrayList<>();
        for (int i=0;i<tareas.size();i++){
            if (!tareas.get(i).estaVencida()){
                tareaNoVencidas.add(tareas.get(i));
            }
        }
        Collections.sort(tareaNoVencidas,(Tarea a,Tarea b) -> a.getPrioridad().compareTo(b.getPrioridad()));
        consola ="***** - LISTA DE TAREAS - *****" + "\n" ;
        for (int i = 0; i < tareaNoVencidas.size(); i++) {
            consola += (i+1) + " - " + tareaNoVencidas.get(i).mostrarTareaPrioridad() + "\n";
        }
        return consola;
    }
    private String mostrarTareaNoVencidadVencimiento(){
        String consola="";
        ArrayList<Tarea> tareaNoVencidas = new ArrayList<>();
        for (int i=0;i<tareas.size();i++){
            if (!tareas.get(i).estaVencida()){
                tareaNoVencidas.add(tareas.get(i));
            }
        }
        Collections.sort(tareaNoVencidas,(Tarea a,Tarea b) -> a.getFechaVencimiento().compareTo(b.getFechaVencimiento()));
        consola ="***** - LISTA DE TAREAS - *****" + "\n" ;
        for (int i = 0; i < tareaNoVencidas.size(); i++) {
            consola += (i+1) + " - " + tareaNoVencidas.get(i).mostrarTareaVencimiento() + "\n";
        }
        return consola;
    }

    private String mostrarTarea(){
        String consola="***** - LISTA DE TAREAS - *****" + "\n" ;
        for (int i = 0; i < tareas.size(); i++) {
            consola += (i+1) + " - " + tareas.get(i).mostrarTarea() + "\n";
        }
        return consola;
    }
    private String mostrarTareaPrioridad(){
        String consola="***** - LISTA DE TAREAS - *****" + "\n" ;
        for (int i = 0; i < tareas.size(); i++) {
            consola += (i+1) + " - Prioridad: " + tareas.get(i).getPrioridad() + " - " + tareas.get(i).mostrarTarea() + "\n";
        }
        return consola;
    }
    private void agregarTarea (){
        System.out.println("INGRESE LA DESCRIPCION DE LA TAREA");
        String descripcion = sc.nextLine();
        System.out.println("INGRESE LA PRIORIDAD: unicas opciones-->(ALTA-MEDIA-BAJA)");
        Prioridad prioridad = Prioridad.valueOf(sc.nextLine());
        System.out.println("INGRESE LA FECHA DE VENCIMIENTO.  FORMATO: AAAA-MM-DD");
        LocalDate vencimiento = LocalDate.parse(sc.next());
        boolean estado = false;
        System.out.println("INGRESE LA FECHA DE RECORDATORIO.  FORMATO: AAAA-MM-DD");
        LocalDate recordatorio = LocalDate.parse(sc.next());
        agregar(descripcion,prioridad,vencimiento,estado,recordatorio);
        sc.nextLine();
        System.out.println("PRESIONE ENTER PARA VOLVER");
        sc.nextLine();

    }
    private void agregar(String descripcion, Prioridad prioridad, LocalDate vencimiento, boolean estado, LocalDate recordatorio){
        Tarea tarea = new Tarea();
        tarea.modificarDescripcion(descripcion);
        tarea.setPrioridad(prioridad);
        tarea.setFechaVencimiento(vencimiento);
        tarea.setEstadoCompleto(estado);
        tarea.setFechaRecordatorio(recordatorio);
        tareas.add(tarea);
    }
    private void modificarDescripcion(){
        System.out.println(mostrarTarea());
        System.out.println("INGRESE EL NUMERO DE TAREA QUE QUIERE MODIFICAR");
        int opcion = sc.nextInt();
        sc.nextLine();
        System.out.println("INGRESE LA NUEVA DESCRIPCION DE TAREA");
        String descripcion = sc.nextLine();
        tareas.get(opcion-1).modificarDescripcion(descripcion);
        System.out.println("PRESIONE ENTER PARA VOLVER");
        sc.nextLine();
    }
    private void modificarPrioridad(){
        System.out.println(mostrarTareaPrioridad());
        System.out.println("INGRESE EL NUMERO DE TAREA QUE QUIERE MODIFICAR");
        int opcion = sc.nextInt();
        sc.nextLine();
        System.out.println("INGRESE LA PRIORIDAD: unicas opciones-->(ALTA-MEDIA-BAJA)");
        Prioridad prioridad = Prioridad.valueOf(sc.nextLine());
        tareas.get(opcion-1).setPrioridad(prioridad);
        System.out.println("PRESIONE ENTER PARA VOLVER");
        sc.nextLine();
    }
    private String mostrarEstado(){
        String consola="***** - LISTA DE TAREAS - *****" + "\n" ;
        for (int i = 0; i < tareas.size(); i++) {
            String estado = "INCONCLUSA";
            if (tareas.get(i).getEstado()){
                estado = "FINALIZADA";
            }
            consola += (i+1) + " - " + tareas.get(i).getDescripcion() + " --> " + estado + "\n";
        }
        return consola;
    }
    private void modificarEstado(){
        System.out.println(mostrarEstado());
        System.out.println("INGRESE EL NUMERO DE TAREA");
        int opcion = sc.nextInt();
        sc.nextLine();
        System.out.println("Opcion: --> 1 FINALIZADA \n" + "Opcion: --> 2 INCONCLUSA");
        int opcion2 = sc.nextInt();

        switch (opcion2){
            case 1:
                tareas.get(opcion - 1).setEstadoCompleto(true);
                tareas.get(opcion - 1).setColaborador(colaborador);
                tareas.get(opcion - 1).setFechaDeFinalizacion(LocalDate.now());
                break;
            case 2:
                tareas.get(opcion - 1).setEstadoCompleto(false);
                tareas.get(opcion - 1).setColaborador("");
                tareas.get(opcion - 1).setFechaDeFinalizacion(null);
                break;
        }
        sc.nextLine();
        System.out.println("PRESIONE ENTER PARA VOLVER");
        sc.nextLine();
    }
    private String mostrarvencida(){
        String consola="***** - LISTA DE TAREAS - *****" + "\n" ;
        for (int i = 0; i < tareas.size(); i++) {
            String estado = "";
            if (tareas.get(i).getFechaVencimiento().isBefore(LocalDate.now())){
                estado = " --> VENCIDA";
            }
            consola += (i+1) + " - " + tareas.get(i).getFechaVencimiento() + " - " + tareas.get(i).getDescripcion() +  estado + "\n";
        }
        return consola;
    }
}