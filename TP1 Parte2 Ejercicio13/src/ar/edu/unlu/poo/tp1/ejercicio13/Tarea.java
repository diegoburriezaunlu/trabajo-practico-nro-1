package ar.edu.unlu.poo.tp1.ejercicio13;
import java.time.*;

public class Tarea {
    private Prioridad prioridad = null ;
    private boolean estaCompleta = false;
    private LocalDate fechaVencimiento;
    private String descripcion = "";
    private LocalDate fechaRecordatorio;
    private LocalDate fechaDeFinalizacion;
    private String colaborador="";

    LocalDate getFechaDeFinalizacion(){
        return this.fechaDeFinalizacion;
    }
    String getColaborador(){
        return this.colaborador;
    }
    void setColaborador(String colaborador){
        this.colaborador = colaborador;
    }
    void setFechaDeFinalizacion(LocalDate fecha){
        this.fechaDeFinalizacion =fecha;
    }
    boolean consultaRecordatorio(){
        if (!estaCompleta && (LocalDate.now().isAfter(getFechaRecordatorio())||LocalDate.now().isEqual(getFechaRecordatorio())) && (LocalDate.now().isBefore(getFechaVencimiento())||LocalDate.now().isEqual(getFechaVencimiento()))){
            return true;
        }else{
            return false;
        }
    }
    void setFechaRecordatorio(LocalDate fecha){
        this.fechaRecordatorio = fecha;
    }
    LocalDate getFechaRecordatorio(){
        return this.fechaRecordatorio;
    }
    boolean estaVencida(){
        if (fechaVencimiento.isBefore(LocalDate.now()) && estaCompleta == false){
            return true;
        }else{
            return false;
        }
    }
    void setPrioridad(Prioridad prioridad) {
        this.prioridad = prioridad;
    }

    void setEstadoCompleto(boolean estado) {
        this.estaCompleta = estado;
    }

    void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    void modificarDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }

    public boolean getEstado() {
        return estaCompleta;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }
    String mostrarTarea(){
        if (estaVencida()){
            return "VENCIDA - " + descripcion;
        } else if (!estaVencida() && estaCompleta){
            return "COMPLETA - " + descripcion;
        }else if (!estaVencida() && !estaCompleta){
            return "INCOMPLETA - " + descripcion;
        }else{
            return "";
        }
    }
    String mostrarTareaPrioridad(){
        return "Prioridad --> "+prioridad + " - " + descripcion;
    }
    String mostrarTareaVencimiento(){
        return "Vencimiento --> "+fechaVencimiento + " - " + descripcion;
    }
    public String getDescripcion() {
        return descripcion;
    }
}
