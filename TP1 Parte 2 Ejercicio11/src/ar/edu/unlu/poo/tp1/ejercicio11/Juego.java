package ar.edu.unlu.poo.tp1.ejercicio11;

import java.util.ArrayList;
import java.util.Scanner;

public class Juego {
    private ArrayList<Jugador> jugadores = new ArrayList<>();
    private ArrayList<Reglamento> reglamentos = new ArrayList<>();
    private ArrayList<String> diccionario = new ArrayList<>();

    Scanner sc = new Scanner(System.in);
    public void nuevoJugador(String nombre){
        Jugador jugador = new Jugador();
        jugador.setNombre(nombre);
        jugadores.add(jugador);
    }
    public void comenzarElJuego() {
        System.out.println("---------------------------------------------------------");
        System.out.println("¿Cuantos jugadores van a participar?");
        int opc = sc.nextInt();
        System.out.println("---------------------------------------------------------");
        for (int i = 0; i < opc; i++) {
            sc.nextLine();
            System.out.println("Nombre del Jugador Nro: " + (i + 1));
            nuevoJugador(sc.nextLine());
            System.out.println("---------------------------------------------------------");
        }
        System.out.println("¿Cuantas palabras van a ingresar por jugador en el juego?");
        int opc1 = sc.nextInt();
        partida(opc1);
        System.out.println(mostrarPuntos());
        //System.out.println("Si quiere volver a jugar precione 1 o ENTER para salir");
        //String opc2 = sc.nextLine();
        //if (opc2.equals(1)){
        //    comenzarElJuego();
        //}
    }
    private String mostrarPuntos() {
        String respuesta = "";
        Jugador ganador=null;
        String nombreGanador="";
        boolean empate =false;
        for (int i = 0; i < jugadores.size(); i++) {
            if (i==0){
                ganador = jugadores.get(0);
                nombreGanador = ganador.getNombre();
            }else if(jugadores.get(i).getPuntos()==ganador.getPuntos()){
                ganador = jugadores.get(i);
                nombreGanador += " y " + ganador.getNombre();
                empate = true;
            }else if (jugadores.get(i).getPuntos()>ganador.getPuntos()){
                ganador = jugadores.get(i);
                nombreGanador = ganador.getNombre();
                empate = false;
            }
            respuesta += String.format("Jugador: " + jugadores.get(i).getNombre() + "\n");
            respuesta += jugadores.get(i).mostrarJugadas() + "\n";
            respuesta += "---------------------------------------\n";
            respuesta += "PUNTOS TOTALES: "+jugadores.get(i).getPuntos() + "\n";
            respuesta += "---------------------------------------\n";
            respuesta += "\n";
        }
        if (empate){
            respuesta += "EMPATARON entre " + nombreGanador;
        }else {
            respuesta += ("El GANADOR es: " + nombreGanador );
        }

        return respuesta;
    }

    private void partida(int cantPalabras){
            cargarReglamento();
            cargarDiccionario();
            System.out.println("");
            System.out.println("");
            System.out.println("---------------------------------------------------------");
            System.out.println("------------------COMIENZO DEL JUEGO---------------------");
            System.out.println("---------------------------------------------------------");
            for (int i = 0; i < cantPalabras; i++) {
                for (int j = 0; j < jugadores.size(); j++) {
                    System.out.println("");
                    System.out.println("");
                    System.out.println("---------------------------------------------------------");
                    System.out.println("      PALABRAS QUE EXISTEN EN EL DICIONARIO:\n" +
                                       "      casa - juego - auto - programa - kiwi - zorro\n" +
                                       "      almanaque - auxilio - payaso - camion - perro \n" +
                                       "      chocolate - jamon - tijera - silla - aula - gato");
                    System.out.println("---------------------------------------------------------");
                    sc.nextLine();
                    System.out.println("Jugador " + jugadores.get(j).getNombre() + " ingrese una palabra numero " + (i + 1) + ":");
                    String palabra = sc.nextLine();
                    jugadores.get(j).agregarJugada(palabra,reglamentos,diccionario);
                }
            }
    }
    private void cargarReglamento(){
        Reglamento letraK = new Reglamento();
        letraK.setLetra('k');
        letraK.setPuntos(2);
        reglamentos.add(letraK);
        Reglamento letraZ = new Reglamento();
        letraZ.setLetra('z');
        letraZ.setPuntos(2);
        reglamentos.add(letraZ);
        Reglamento letraX = new Reglamento();
        letraX.setLetra('x');
        letraX.setPuntos(2);
        reglamentos.add(letraX);
        Reglamento letraY = new Reglamento();
        letraY.setLetra('y');
        letraY.setPuntos(2);
        reglamentos.add(letraY);
        Reglamento letraW = new Reglamento();
        letraW.setLetra('w');
        letraW.setPuntos(2);
        reglamentos.add(letraW);
    }
    private void cargarDiccionario(){
        diccionario.add("casa");
        diccionario.add("juego");
        diccionario.add("auto");
        diccionario.add("programa");
        diccionario.add("kiwi");
        diccionario.add("zorro");
        diccionario.add("almanaque");
        diccionario.add("auxilio");
        diccionario.add("payaso");
        diccionario.add("camion");
        diccionario.add("pero");
        diccionario.add("chocolate");
        diccionario.add("jamon");
        diccionario.add("tijera");
        diccionario.add("silla");
        diccionario.add("aula");
        diccionario.add("gato");
    }
}
