package ar.edu.unlu.poo.tp1.ejercicio11;

import java.util.ArrayList;

public class Jugada {
    private String palabra = "";
    private int puntos=0;
    String getPalabra(){
        return this.palabra;
    }
    int getPuntos(){
        return this.puntos;
    }
    private void setPuntos(int puntos){
        this.puntos = puntos;
    }
    int setPalabra(String palabra, ArrayList<Reglamento> reglamentos, ArrayList<String> diccionario){
        this.palabra = palabra;
        setPuntos(calcularPuntos(palabra,reglamentos,diccionario));
        return getPuntos();
    }
    private int calcularPuntos(String palabra, ArrayList<Reglamento> reglamentos, ArrayList<String> diccionario){
        int suma=0;
        palabra = palabra.toLowerCase();
        for(int i=0;i<diccionario.size();i++){
            if (palabra.equals(diccionario.get(i))){
                for (int j=0;j<palabra.length();j++){
                    suma ++;
                    for (int k=0;k<reglamentos.size();k++){
                        char letra = palabra.charAt(j);
                        if (letra == reglamentos.get(k).getLetra()){
                            suma += reglamentos.get(k).getPuntos()-1;
                        }
                    }
                }
            }
        }
        return suma;
    }

}
