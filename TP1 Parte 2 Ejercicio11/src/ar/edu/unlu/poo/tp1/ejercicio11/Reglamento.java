package ar.edu.unlu.poo.tp1.ejercicio11;

import java.util.ArrayList;

public class Reglamento {
    private char letra ;
    private int puntos =0;
    void setLetra(char letra){
        this.letra=letra;
    }
    void setPuntos(int puntos){
        this.puntos=puntos;
    }
    int getPuntos(){
        return this.puntos;
    }
    char getLetra(){
        return this.letra;
    }
}
