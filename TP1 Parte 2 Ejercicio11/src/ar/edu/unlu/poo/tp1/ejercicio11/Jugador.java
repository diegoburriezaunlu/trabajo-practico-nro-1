package ar.edu.unlu.poo.tp1.ejercicio11;

import java.util.ArrayList;

public class Jugador {
    private String nombre;
    private int puntos = 0;
    private ArrayList<Jugada> jugadas = new ArrayList<>();
    void setNombre(String nombre){
        this.nombre = nombre;
    }
    String getNombre (){
        return this.nombre;
    }
    int obtenerPuntosTotal(){
        return this.puntos;
    }
    void setPuntos(int puntaje){
        this.puntos += puntaje;
    }
    void agregarJugada(String palabra, ArrayList<Reglamento> reglamentos, ArrayList<String> diccionario){
        Jugada jugada = new Jugada();
        setPuntos(jugada.setPalabra(palabra,reglamentos,diccionario));
        jugadas.add(jugada);
    }
    String mostrarJugadas(){
        String respuesta ="";
        for (int i =0;i<jugadas.size();i++){
            respuesta += jugadas.get(i).getPalabra() + " ---> " +jugadas.get(i).getPuntos()+"\n";
        }
        return respuesta;
    }
    int getPuntos(){
        return this.puntos;
    }

}
