import ar.edu.unlu.poo.tp1.ejercicio11.Juego;
import ar.edu.unlu.poo.tp1.ejercicio11.Jugador;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Juego juego = new Juego();
        juego.comenzarElJuego();
    }
}