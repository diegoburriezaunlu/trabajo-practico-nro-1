import ar.edu.unlu.poo.tp1.eje6.Aplicacion;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Aplicacion app = new Aplicacion();
        app.cargaInicial();
        app.menu();
    }
}