package ar.edu.unlu.poo.tp1.eje6;
import java.time.LocalDate;
import java.util.*;
public class Aplicacion {
    private ArrayList<Libro> libros = new ArrayList<Libro>();
    Scanner sc = new Scanner(System.in);
    public void cargaInicial() {
        agregarLibro("Programacion con JAVA", "Diego B", 444,7);
        agregarLibro("Juegos con CARTAS redondas", "Antonio Redondo", 1350,3);
        agregarLibro("Reparando bocinas de aviones", "Arturo Vuela", 14,0);
        agregarLibro("casa", "roco", 50,5);
        agregarEjemplarTitulo(3, "Programacion con JAVA");
        agregarEjemplarTitulo(18, "Juegos con CARTAS redondas");
        agregarEjemplarTitulo(1, "Reparando bocinas de aviones");
    }
    public void menu(){
        int opcion;
        boolean salir = false;
        while (!salir) {
            System.out.println("***************************************************************");
            System.out.println("          1. Mostrar Libros");
            System.out.println("          2. Buscar Libro (Autor o Titulo)");
            System.out.println("          3. Prestar libro");
            System.out.println("          4. Consultar cantidad de libros prestados");
            System.out.println("          5. Cual libro tiene mas paginas");
            System.out.println("          6. PRESTAMO PEDIDO EN EL TP Libro de 10 ejemplares");
            System.out.println("          7. PRESTAMO PEDIDO EN EL TP Libro de 1 ejemplares");
            System.out.println("          0. Salir");
            System.out.println("***************************************************************");
            System.out.println("Elija una de las opciones");

            opcion = sc.nextInt();

            switch (opcion) {
                case 0:
                    salir = true;
                    break;
                case 1:
                    mostrarLibros(libros);
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 2:
                    buscarLibro(libros);
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    break;
                case 3:
                    if (prestarLibro(libros)){
                        System.out.println("");
                        System.out.println("-------------------------------");
                        System.out.println("El Libro pudo ser prestado");
                        System.out.println("-------------------------------");
                        System.out.println("");
                    }else{
                        System.out.println("");
                        System.out.println("-------------------------------");
                        System.out.println("NO se puede prestar el libro");
                        System.out.println("-------------------------------");
                        System.out.println("");
                    }
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    break;
                case 4:
                    System.out.println("-----------------------------------------------------");
                    System.out.println("La cantidad de libros que se prestaron fueron: " + cantLibrosPrestados(libros));
                    System.out.println("-----------------------------------------------------");
                    System.out.println("");
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 5:
                    String libroPaginas = cualTieneMasPaginas(libros);
                    System.out.println("-----------------------------------------------------");
                    System.out.println(libroPaginas);
                    System.out.println("-----------------------------------------------------");
                    System.out.println("");
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 6:
                    prestamo1PedidoTp();
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    break;
                case 7:
                    prestamo2PedidoTp();
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    break;
                default:
                    System.out.println("Solo numeros entre 0 y 7");

            }
        }
    }
    private void agregarLibro (String titulo, String autor, int paginas, int ejemplares){
        Libro nuevo = new Libro();
        nuevo.setTitulo(titulo);
        nuevo.setAutor(autor);
        nuevo.setPaginas(paginas);
        nuevo.setEjemplares(ejemplares);
        nuevo.setPrestados(0);
        nuevo.setQuedan(ejemplares);
        libros.add(nuevo);
    }
    private ArrayList<Libro> buscarLibro(ArrayList<Libro> elementos){
        ArrayList<Libro> encontrados = new ArrayList<Libro>();
        System.out.println("¿COMO QUIERE BUSCAR EL LIBRO?");
        System.out.println("Elija la opcion: ");
        System.out.println("1 --> TITULO");
        System.out.println("2 --> AUTOR");
        int opcion = sc.nextInt();
        if (opcion == 1){
            sc.nextLine();
            System.out.println("INGRESE EL TITULO");
            String titulo = sc.nextLine();
            encontrados = buscarPorTitulo(elementos, titulo);

        }else if (opcion == 2){
            sc.nextLine();
            System.out.println("INGRESE EL AUTOR");
            String autor = sc.nextLine();
            encontrados = buscarPorAutor(elementos, autor);
        }
        if (encontrados.isEmpty()){
            System.out.println("NO SE ENCONTRO NINGUN LIBRO CON ESA DESCRIPCION!!");
        }else{
            mostrarLibros(encontrados);
        }
        return encontrados;
    }
    private ArrayList<Libro> buscarPorTitulo(ArrayList<Libro> elementos, String titulo){
        ArrayList<Libro> respuesta = new ArrayList<Libro>();
        for (int i =0;i<elementos.size();i++){
            if ( elementos.get(i).getTitulo().equals(titulo)){
                respuesta.add(elementos.get(i));
            }
        }
        return respuesta;
    }
    private ArrayList<Libro> buscarPorAutor(ArrayList<Libro> elementos, String autor){
        ArrayList<Libro> respuesta = new ArrayList<Libro>();
        for (int i =0;i<elementos.size();i++){
            if (elementos.get(i).getAutor().equals(autor)){
                respuesta.add(elementos.get(i));
            }
        }
        return respuesta;
    }
    private void agregarEjemplarTitulo (int cantidad, String titulo) {
       for (int i = 0; i<libros.size();i++) {
           if (libros.get(i).getTitulo() == titulo) {
               libros.get(i).setEjemplares(libros.get(i).getEjemplares() + cantidad);
               libros.get(i).setQuedan(libros.get(i).getQuedan() + cantidad);
           }
       }
    }
    private boolean prestarLibro(ArrayList<Libro> elementos){
        ArrayList<Libro> encontrados = new ArrayList<Libro>();
        encontrados = buscarLibro(elementos);
        if (!encontrados.isEmpty()){
            System.out.println("ELIJA EL NRO DE LIBRO A PRESTAR");
            boolean resp = encontrados.get(Integer.valueOf(sc.nextLine())-1).prestarUnLibro();
            return resp;
        }else{
            return false;
        }

    }
    private int cantLibrosPrestados(ArrayList<Libro> elementos){
        int respuesta=0;
        for (int i=0;i<elementos.size();i++){
            respuesta += elementos.get(i).getPrestados();
        }
        return respuesta;
    }
    private void mostrarLibros(ArrayList<Libro> elementos){
        System.out.println("----------------------------------------------------------------------------------------------------------------------------");
        System.out.printf("%-5s %-30s %-20s %-17s %-17s %-17s %-17s", "NRO", "TITULO", "AUTOR", "CANT-PAGINAS - ", "Ejemplares - ", "Prestados - " ,
                                                       "Sin prestar" );
        System.out.println();
        System.out.println("----------------------------------------------------------------------------------------------------------------------------");
        for (int i = 0; i<elementos.size();i++) {
            System.out.format("%-5d %-30s %-20s %-17d %-17d %-17d  %-17d", i+1,
                    elementos.get(i).getTitulo(), elementos.get(i).getAutor(), elementos.get(i).getPaginas(),
                    elementos.get(i).getEjemplares(),elementos.get(i).getPrestados(), elementos.get(i).getQuedan());
            System.out.println();
        }
        System.out.println("----------------------------------------------------------------------------------------------------------------------------");
    }
    private String cualTieneMasPaginas(ArrayList<Libro> elementos){
        mostrarLibros(elementos);
        System.out.println("Elegir LIBRO 1 a comprar. --> **INGRESE EL NRO DE LIBRO**");
        int opc1 = sc.nextInt();
        System.out.println("Elegir LIBRO 2 a comprar. --> **INGRESE EL NRO DE LIBRO**");
        int opc2 = sc.nextInt();
        opc1 --;
        opc2 --;
        String respuesta;
        if (libros.get(opc1).getPaginas() > libros.get(opc2).getPaginas()){
            respuesta = "El que mas pagina tiene es: " + "#" + libros.get(opc1).getTitulo() + "#"  + " y tiene " + libros.get(opc1).getPaginas() + " paginas";
        } else if(libros.get(opc2).getPaginas() > libros.get(opc1).getPaginas()){
            respuesta = "El que mas pagina tiene es: " + libros.get(opc2).getTitulo() + " y tiene " + libros.get(opc2).getPaginas() + " paginas";
        }else{
            respuesta = "Tienen la misma cantidad de paginas";
        }
        return respuesta;
    }
    private void prestamo1PedidoTp(){
        if (libros.get(1).prestarUnLibro()){
            System.out.println("");
            System.out.println("El libro pudo ser prestado");
            System.out.println("");
            sc.nextLine();
        }else{
            System.out.println("");
            System.out.println("El libro NO puede ser prestado");
            System.out.println("");
            sc.nextLine();

        }
    }
    private void prestamo2PedidoTp(){
        if (libros.get(2).prestarUnLibro()){
            sc.nextLine();
            System.out.println("El libro pudo ser prestado");
        }else{
            System.out.println("El libro NO puede ser prestado");
            sc.nextLine();
        }
    }
}
