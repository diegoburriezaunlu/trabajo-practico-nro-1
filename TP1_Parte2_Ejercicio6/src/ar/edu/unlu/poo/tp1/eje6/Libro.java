package ar.edu.unlu.poo.tp1.eje6;

public class Libro {
    private String titulo;
    private String autor;
    private int paginas;
    private int ejemplares;
    private int prestados;
    private int quedan;
    private String isbn;

    String mostrarLibro(){
        return titulo + " - " + autor + " - " + paginas + " - " + quedan + " - " + prestados;
    }
    boolean prestarUnLibro(){
        if (quedan > 1){
            quedan --;
            prestados ++;
            return true;
        }else{
            return false;
        }
    }

    String getTitulo() {
        return titulo;
    }

    void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    String getAutor() {
        return autor;
    }

    void setAutor(String autor) {
        this.autor = autor;
    }

    int getPaginas() {
        return paginas;
    }

    void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    int getEjemplares() {
        return ejemplares;
    }

    void setEjemplares(int ejemplares) {
        this.ejemplares = ejemplares;
    }

    int getPrestados() {
        return prestados;
    }

    void setPrestados(int prestados) {
        this.prestados = prestados;
    }

    int getQuedan() {
        return quedan;
    }

    void setQuedan(int quedan) {
        this.quedan = quedan;
    }


}
