package ar.edu.unlu.poo.tp1.ejercicio9;
import java.time.LocalDate;
public class Fecha {
    private String fecha;
    public String transformaFormatoDiaMesAño(String diaMesAño){
      String[] partes = diaMesAño.split("/");
      String respuesta = partes[0] + "-";
      respuesta += partes[1] + "-";
      respuesta += partes[2];
      return respuesta;
    }
    public String transformaFormatoMesDiaAño(String diaMesAño){
        String[] partes = diaMesAño.split("/");
        String respuesta = partes[1] + "-";
        respuesta += partes[0] + "-";
        respuesta += partes[2];
        return respuesta;
    }
    public String transformaFormatoAñoMesDia(String diaMesAño){
        String[] partes = diaMesAño.split("/");
        String respuesta = partes[2] + "-";
        respuesta += partes[1] + "-";
        respuesta += partes[0];
        return respuesta;
    }
    public String determinarSiEstaEntreFechas(String fecha, String fecha1, String fecha2){
        LocalDate lFecha = LocalDate.parse(transformaFormatoAñoMesDia(fecha));
        LocalDate lFecha1 = LocalDate.parse(transformaFormatoAñoMesDia(fecha1));
        LocalDate lFecha2 = LocalDate.parse(transformaFormatoAñoMesDia(fecha2));
        if (lFecha1.isAfter(lFecha) && lFecha2.isBefore(lFecha)||lFecha1.isBefore(lFecha) &&
                lFecha2.isAfter(lFecha)){
            return "SI se encuentra entre las dos fechas.";
        }else{
            return "NO se encuentra entre las dos fechas.";
        }
    }
    public String anterior(String fechaAnt,String fecha ){
        LocalDate lFechaAnt = LocalDate.parse(transformaFormatoAñoMesDia(fechaAnt));
        LocalDate lFecha = LocalDate.parse(transformaFormatoAñoMesDia(fecha));
        if (lFechaAnt.isBefore(lFecha)){
            return fechaAnt + " es ANTERIOR a "+ fecha;
        }else{
            return fechaAnt + " NO es ANTERIOR a "+ fecha;
        }
    }
    public String posterior(String fechaPos,String fecha){
        LocalDate lFechaPos = LocalDate.parse(transformaFormatoAñoMesDia(fechaPos));
        LocalDate lFecha = LocalDate.parse(transformaFormatoAñoMesDia(fecha));
        if (lFechaPos.isAfter(lFecha)){
            return fechaPos + " es POSTERIOR a "+ fecha;
        }else{
            return fechaPos + " NO es POSTERIOR a "+ fecha;
        }
    }
}
