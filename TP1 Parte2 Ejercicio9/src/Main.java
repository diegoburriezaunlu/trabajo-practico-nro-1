import ar.edu.unlu.poo.tp1.ejercicio9.Fecha;

public class Main {
    public static void main(String[] args) {
        System.out.println("");
        System.out.println("--------------------------------------------------------");
        Fecha fecha = new Fecha();
        System.out.println("Fecha Original: 25/09/1982");
        System.out.println("Transformada a DD-MM-YYYY --> "+fecha.transformaFormatoDiaMesAño("25/09/1982"));
        System.out.println("Transformada a MM-DD-YYYY --> "+fecha.transformaFormatoMesDiaAño("25/09/1982"));
        System.out.println("--------------------------------------------------------");
        System.out.println("Fecha Original: 04/03/2021");
        System.out.println("Voy a verificar si esta entre 03/03/2023 y 25/12/2028");
        System.out.println(fecha.determinarSiEstaEntreFechas("04/03/2021", "03/03/2021","25/12/2028"));
        System.out.println("--------------------------------------------------------");
        System.out.println("Fecha Original: 04/03/2021");
        System.out.println("Voy a verificar si esta entre 03/05/2023 y 25/12/2028");
        System.out.println(fecha.determinarSiEstaEntreFechas("04/03/2021", "03/05/2021","25/12/2028"));
        System.out.println("--------------------------------------------------------");
        System.out.println("Verificar si  03/05/2023 es ANTERIOR 25/12/2028");
        System.out.println(fecha.anterior("03/05/2023","25/12/2028"));
        System.out.println("--------------------------------------------------------");
        System.out.println("Verificar si  25/12/2028 es ANTERIOR 03/05/2023");
        System.out.println(fecha.anterior("25/12/2028","03/05/2023"));
        System.out.println("--------------------------------------------------------");
        System.out.println("Verificar si  03/05/2023 es POSTERIOR 25/12/2028");
        System.out.println(fecha.posterior("03/05/2023","25/12/2028"));
        System.out.println("--------------------------------------------------------");
        System.out.println("Verificar si  25/12/2028 es POSTERIOR 03/05/2023");
        System.out.println(fecha.posterior("25/12/2028","03/05/2023"));
        System.out.println("--------------------------------------------------------");

    }
}