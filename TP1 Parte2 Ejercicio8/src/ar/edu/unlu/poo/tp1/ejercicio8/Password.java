package ar.edu.unlu.poo.tp1.ejercicio8;

import java.util.Random;

public class Password {
    private String password="";
    private void setPassword(String password){
        this.password =password;
    }
    String getpassword(){
        return this.password;
    }
    void generarPassword(int cant){
        if (cant <8){
            cant = 8;
        }
        String alphabet = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789";
        Random r = new Random();
        String password = "";
        for (int i =0;i<cant;i++){
            char caracter = alphabet.charAt(r.nextInt(alphabet.length()));
            password += caracter;
        }
        setPassword(password);
    }
    String mostrarPass(){
        return getpassword();
    }
    String evaluarClave(){
        String password = getpassword();
        String caracter="";
        int contMayusculas=0;
        int contMinusculas=0;
        int contNumeros=0;
        for (int i = 0;i<password.length();i++){
            caracter = String.valueOf(password.charAt(i));
            if (caracter.equals(caracter.toUpperCase())){
                contMayusculas ++;
            }
            if (caracter.equals(caracter.toLowerCase())){
                contMinusculas++;
            }
            if (caracter.matches("[0-9]+")){
                contNumeros++;
            }
        }
        if (contMayusculas>=2 && contMinusculas>=1 && contNumeros>=2){
            return "Fuerte";
        }else{
            return "Débil";
        }
    }
}
