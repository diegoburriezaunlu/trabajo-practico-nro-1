package ar.edu.unlu.poo.tp1.ejercicio8;

import java.util.*;

public class Aplicacion {
    private ArrayList<Password> passwords = new ArrayList<>();
    Scanner sc = new Scanner(System.in);
    public void menu(){
        int opcion;
        boolean salir = false;
        while (!salir) {
            System.out.println("*******************************************");
            System.out.println("          1. Generar NUEVA PASSWORD");
            System.out.println("          2. Mostrar PASSWORD");
            System.out.println("          3. Regenerar PASSWORD");
            System.out.println("          0. Salir");
            System.out.println("*******************************************");
            System.out.println("Elija una de las opciones");

            opcion = sc.nextInt();

            switch (opcion) {
                case 0:
                    salir = true;
                    break;
                case 1:
                    System.out.println("Ingrese la cantidad de caracteres deceados: ");
                    int opc = sc.nextInt();
                    System.out.println("");
                    nuevaClave(opc);
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 2:
                    System.out.println(mostarPasswords());
                    System.out.println("");
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 3:
                    regenerarClave();
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                default:
                    System.out.println("Solo numeros entre 0 y 7");
            }
        }
    }
    void nuevaClave(int cant){
        Password nuevaPass = new Password();
        nuevaPass.generarPassword(cant);
        passwords.add(nuevaPass);
    }
    void nuevaClave(int opc, int cant){
        Password nuevaPass = new Password();
        nuevaPass.generarPassword(cant);
        passwords.add(opc,nuevaPass);
    }
    String mostarPasswords(){
        String respuesta = "";
        respuesta += String.format("------------------------------------------\n");
        respuesta += String.format("%-5s %-20s %-10s", "NRO", "PASSWORD", "CALIFICACION" + "\n");
        respuesta += String.format("------------------------------------------\n");
        for (int i = 0; i<passwords.size();i++) {
            respuesta += String.format("%-5d %-20s %-10s", i+1, passwords.get(i).mostrarPass(), passwords.get(i).evaluarClave(), "\n");
            respuesta += String.format("\n");
        }
        respuesta += String.format("------------------------------------------");

        return respuesta;
    }
    void regenerarClave(){
        System.out.println(mostarPasswords());
        System.out.println("INGRESE EL NUMERO DE CLAVE QUE QUIERE REGENERAR: ");
        int opc = sc.nextInt();
        System.out.println("INGRESE EL NUMERO DE CARACTERES: ");
        int opc2 = sc.nextInt();
        passwords.remove(opc-1);
        nuevaClave(opc-1, opc2);
    }
}
