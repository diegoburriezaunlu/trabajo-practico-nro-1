package ar.edu.unlu.poo.tp1.ej5;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class App {
    private List<Tarea> tareas = new ArrayList<>();
    private Scanner sc = new Scanner(System.in);

    public void mostarMenu(){
        Scanner sc = new Scanner(System.in);
        int opcion;
        boolean salir = false;
        agregar("Ir al supermercado mañana", Prioridad.ALTA,LocalDate.now(), false);
        agregar("Consultar repuesto del auto", Prioridad.MEDIA,LocalDate.now().minusDays(1), true);
        agregar("Ir al cine a ver la nueva película de Marvel", Prioridad.BAJA,LocalDate.now().minusDays(1), false);
        while (!salir) {
            System.out.println("****************************************************");
            System.out.println("          1. Mostrar tareas");
            System.out.println("          2. Agregar una tarea");
            System.out.println("          3. Modificar descripcion");
            System.out.println("          4. Modificar prioridad");
            System.out.println("          5. Marcar como FINALIZADA o INCONCLUSA");
            System.out.println("          6. Consultar cuales estan FINALIZADAS");
            System.out.println("          7. Consultar cuales estan VENCIDAS");
            System.out.println("          0. Salir");
            System.out.println("****************************************************");
            System.out.println("Elija una de las opciones");

            opcion = sc.nextInt();

            switch (opcion) {
                case 0:
                    salir = true;
                    break;
                case 1:
                    System.out.println(mostrarTarea());
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                case 2:
                    agregarTarea();
                    break;
                case 3:
                    modificarDescripcion();
                    break;
                case 4:
                    modificarPrioridad();
                    break;
                case 5:
                    modificarEstado();
                    break;
                case 6:
                    System.out.println(mostrarEstado());
                    sc.nextLine();
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    break;
                case 7:
                    System.out.println(mostrarvencida());
                    System.out.println("PRESIONE ENTER PARA VOLVER");
                    sc.nextLine();
                    sc.nextLine();
                    break;
                default:
                    System.out.println("Solo numeros entre 1 y 10");

            }
        }
    }
    private String mostrarTarea(){
        String consola="***** - LISTA DE TAREAS - *****" + "\n" ;
        for (int i = 0; i < tareas.size(); i++) {
            consola += (i+1) + " - " + tareas.get(i).mostrarTarea() + "\n";
        }
        return consola;
    }
    private String mostrarTareaPrioridad(){
        String consola="***** - LISTA DE TAREAS - *****" + "\n" ;
        for (int i = 0; i < tareas.size(); i++) {
            consola += (i+1) + " - Prioridad: " + tareas.get(i).getPrioridad() + " - " + tareas.get(i).mostrarTarea() + "\n";
        }
        return consola;
    }
    private void agregarTarea (){
        System.out.println("INGRESE LA DESCRIPCION DE LA TAREA");
        String descripcion = sc.nextLine();
        System.out.println("INGRESE LA PRIORIDAD: unicas opciones-->(ALTA-MEDIA-BAJA)");
        Prioridad prioridad = Prioridad.valueOf(sc.nextLine());
        System.out.println("INGRESE LA FECHA DE VENCIMIENTO.  FORMATO: AAAA-MM-DD");
        LocalDate vencimiento = LocalDate.parse(sc.next());
        boolean estado = false;
        agregar(descripcion,prioridad,vencimiento,estado);
        sc.nextLine();
        System.out.println("PRESIONE ENTER PARA VOLVER");
        sc.nextLine();

    }
    private void agregar(String descripcion, Prioridad prioridad, LocalDate vencimiento, boolean estado){
        Tarea tarea = new Tarea();
        tarea.modificarDescripcion(descripcion);
        tarea.setPrioridad(prioridad);
        tarea.setFechaVencimiento(vencimiento);
        tarea.setEstadoCompleto(estado);
        tareas.add(tarea);
    }
    private void modificarDescripcion(){
        System.out.println(mostrarTarea());
        System.out.println("INGRESE EL NUMERO DE TAREA QUE QUIERE MODIFICAR");
        int opcion = sc.nextInt();
        sc.nextLine();
        System.out.println("INGRESE LA NUEVA DESCRIPCION DE TAREA");
        String descripcion = sc.nextLine();
        tareas.get(opcion-1).modificarDescripcion(descripcion);
        System.out.println("PRESIONE ENTER PARA VOLVER");
        sc.nextLine();
    }
    private void modificarPrioridad(){
        System.out.println(mostrarTareaPrioridad());
        System.out.println("INGRESE EL NUMERO DE TAREA QUE QUIERE MODIFICAR");
        int opcion = sc.nextInt();
        sc.nextLine();
        System.out.println("INGRESE LA PRIORIDAD: unicas opciones-->(ALTA-MEDIA-BAJA)");
        Prioridad prioridad = Prioridad.valueOf(sc.nextLine());
        tareas.get(opcion-1).setPrioridad(prioridad);
        System.out.println("PRESIONE ENTER PARA VOLVER");
        sc.nextLine();
    }
    private String mostrarEstado(){
        String consola="***** - LISTA DE TAREAS - *****" + "\n" ;
        for (int i = 0; i < tareas.size(); i++) {
            String estado = "INCONCLUSA";
            if (tareas.get(i).getEstado()){
                estado = "FINALIZADA";
            }
            consola += (i+1) + " - " + tareas.get(i).getDescripcion() + " --> " + estado + "\n";
        }
        return consola;
    }
    private void modificarEstado(){
        System.out.println(mostrarEstado());
        System.out.println("INGRESE EL NUMERO DE TAREA");
        int opcion = sc.nextInt();
        sc.nextLine();
        System.out.println("Opcion: --> 1 FINALIZADA \n" + "Opcion: --> 2 INCONCLUSA");
        int opcion2 = sc.nextInt();

        switch (opcion2){
            case 1:
                tareas.get(opcion - 1).setEstadoCompleto(true);
                break;
            case 2:
                tareas.get(opcion - 1).setEstadoCompleto(false);
                break;
        }
        sc.nextLine();
        System.out.println("PRESIONE ENTER PARA VOLVER");
        sc.nextLine();
    }
    private String mostrarvencida(){
        String consola="***** - LISTA DE TAREAS - *****" + "\n" ;
        for (int i = 0; i < tareas.size(); i++) {
            String estado = "";
            if (tareas.get(i).getFechaVencimiento().isBefore(LocalDate.now())){
                estado = " --> VENCIDA";
            }
            consola += (i+1) + " - " + tareas.get(i).getFechaVencimiento() + " - " + tareas.get(i).getDescripcion() +  estado + "\n";
        }
        return consola;
    }
}
