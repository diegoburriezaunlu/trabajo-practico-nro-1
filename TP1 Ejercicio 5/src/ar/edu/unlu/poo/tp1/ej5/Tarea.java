package ar.edu.unlu.poo.tp1.ej5;
import java.time.*;

public class Tarea {
    private Prioridad prioridad = null ;
    private boolean estaCompleta = false;
    private LocalDate fechaVencimiento;
    private String descripcion = "";

    boolean estaVencida(){
        if (fechaVencimiento.isBefore(LocalDate.now()) && estaCompleta == false){
            return true;
        }else{
            return false;
        }
    }
    void setPrioridad(Prioridad prioridad) {
            this.prioridad = prioridad;
    }

    void setEstadoCompleto(boolean estado) {
        this.estaCompleta = estado;
    }

    void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    void modificarDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }

    public boolean getEstado() {
        return estaCompleta;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }
    String mostrarTarea(){
        if (estaVencida()){
            return "VENCIDA - " + descripcion;
        } else if (!estaVencida() && estaCompleta){
            return "COMPLETA - " + descripcion;
        }else if (!estaVencida() && !estaCompleta){
            return "INCOMPLETA - " + descripcion;
        }else{
            return "";
        }

    }
    public String getDescripcion() {
        return descripcion;
    }
}
/*  import java.time.*; // Este paquete contiene LocalDate, LocalTime y LocalDateTime.
    import java.time.format.*;  // Este paquete contiene DateTimeFormatter.

        LocalDate hoy = LocalDate.now();
        LocalTime ahora = LocalTime.now();
        LocalDateTime fecha = LocalDateTime.now();
        LocalDateTime fecha = LocalDateTime.of(hoy, ahora);
        año-mes-dia

 */